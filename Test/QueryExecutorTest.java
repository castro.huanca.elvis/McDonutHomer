import com.projetc.mcdonut.model.*;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static com.projetc.mcdonut.model.database.QueryExecutor.updateRegisters;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class QueryExecutorTest {



    @Test
    public void insertCustomerTest(){

        int updatedRegisters = 0;

        Date date = new Date(2001, 12,12);
        Customer customer = new Customer("Juanito Alcachofa", "123", "juan@gmail.com", date, 123);
        Customer customer2 = new Customer("Julián Alvarez", "129daA", "alvarez@gmail.com", date, 4712837);


        updatedRegisters += updateRegisters(Customer.SQL_INSERT_CUSTOMER, customer, Query.INSERT);
        updatedRegisters += updateRegisters(Customer.SQL_INSERT_CUSTOMER, customer2, Query.INSERT);

        assertEquals(2, updatedRegisters);
    }

    @Test
    public void updateCustomerTest(){

        Date date = new Date(2001, 12,12);
        Customer updatedCustomer = new Customer("Juanito Perez", "123", "juan@gmail.com", date, 123, 2);

        int updatedRegisters = updateRegisters(Customer.SQL_UPDATE_CUSTOMER, updatedCustomer, Query.UPDATE);

        assertEquals(1, updatedRegisters);

    }

    @Test
    public void insertProductTest(){

        int updatedRegisters = 0;

        Product donut = new Product("This is a donut", "Donut", "PRINCIPAL", 15);
        Product donut2 = new Product("This is another donut", "Donut", "PRINCIPAL", 23);

        updatedRegisters += updateRegisters(Product.SQL_INSERT_PRODUCT, donut, Query.INSERT);
        updatedRegisters += updateRegisters(Product.SQL_INSERT_PRODUCT, donut2, Query.INSERT);

        assertEquals(2, updatedRegisters);
    }

    @Test
    public void updateProductTest(){

        Product updatedDonut = new Product("This is a chocolate donut", "Donut", "PRINCIPAL", 20,1000);

        int updatedRegisters = updateRegisters(Product.SQL_UPDATE_PRODUCT, updatedDonut, Query.UPDATE);

        assertEquals(1, updatedRegisters);
    }

}