import com.projetc.mcdonut.controller.SignUpController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

public class SignUpTest {

    SignUpController signUpController = new SignUpController();

    @Test
    void changeStringToDate(){
        Date date = signUpController.changeStringToDate("2001-03-04");

        Date dateExpected = new Date(2001, 03, 04);

        assertEquals (dateExpected, date);
    }

}
