# McDonutHomer
# Mc Donuts README

![Logo](https://i.postimg.cc/vZ0sg9t5/fondo-bebidas.jpg)

_> The best Donuts_

###Table of Contents
- [Description](#description)
- [Technologies](#technologies)
- [Extras](#extras)
- [Authors and Acknowledgement](#authors-and-acknowledgement)
- [Problems we have](#extras)

---
## Description
We organize ourselves through the "Notion" platform, in which we manage to divide ourselves into user stories depending on their priority.
---


## Technologies
- Java 11
- Markdown
- IntelliJ IDEA
- Git
---
## Extras
As something extra, we add a scene where you can buy extra products such as jelly or flan

## Problems
At the beginning we had problems with the distribution of classes, since for each model we had a view and controller. We also had many problems with dependencies and all the stuff about gitlab



## Authors and Acknowledgement
This project was made by the group "ELVISCOCHO", whose members are:

- Gabriela Garcia
- Victor Villca
- Elvis Castro
- Salet Gutierrez

We acknowledge Victor Chavez as our great leader and educator.
