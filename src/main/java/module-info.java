module com.projetc.mcdonut {
    
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens com.projetc.mcdonut to javafx.fxml;
    exports com.projetc.mcdonut;
    exports com.projetc.mcdonut.controller;
    exports com.projetc.mcdonut.model;
    exports com.projetc.mcdonut.model.database;
    opens com.projetc.mcdonut.controller to javafx.fxml;

}