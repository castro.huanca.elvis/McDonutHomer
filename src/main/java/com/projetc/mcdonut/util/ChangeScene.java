package com.projetc.mcdonut.util;

import com.projetc.mcdonut.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class ChangeScene {

    private static ChangeScene changeScene;

    /**
     * We implement the singleton.
     *
     * To no longer repeat the class in ChangeScene
     * then we implement the design pattern that
     * is the singleton to reduce code.
     * @return retorna la variable de changeScene
     */

    public static ChangeScene getChangeScene(){
        if (changeScene == null){
            changeScene= new ChangeScene();
        }
        return changeScene;
    }

    /**
     *This method gives the action on the button to change the screen by pressing it.
     *
     * It will help us that the button that has this action
     * will go to another screen to carry out its actions
     * properly from that panel.
     *
     * @param event this is the parameter of the action to load the event.
     * @param nameScene this parameter will help us to put the name of the FXML.
     * @param title with this parameter we can put a title to the panel.
     * @throws IOException is an exception.
     */

    public void changeScreenButtonPushed(ActionEvent event, String nameScene, String title) throws IOException {

        try{
            Parent loadScene= FXMLLoader.load(Main.class.getResource(nameScene));
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setTitle(title);
            window.setScene(new Scene(loadScene, 897, 634));
            window.show();
        }catch (IOException exception){
            exception.printStackTrace();
        }
    }

}
