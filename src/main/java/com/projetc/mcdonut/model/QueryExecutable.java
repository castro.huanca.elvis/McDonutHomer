package com.projetc.mcdonut.model;

import java.sql.*;
import java.util.ArrayList;

/**
 * This is an interface class that has as methods
 * the types of actions that can be performed
 * between the project and the database.
 *
 * Any class that implements this class must override
 * the methods so that they have a different behavior for each class.
 */
public interface QueryExecutable{

    /**
     * This method fulfills the function of obtaining data from the database.
     *
     * Each class that implements this interface will
     * override this method to execute the data
     * retrieval with different behaviors, depending on the class.
     *
     * @param result for get data.
     * @return Arraylist of objects obtained from data in the database.
     * @throws SQLException if a database access error occurs.
     */
    public ArrayList<Object> getData(ResultSet result) throws SQLException;

    /**
     * This method fulfills the function of adding data to the database.
     *
     * Each class that implements this interface will
     * override this method to perform data aggregation
     * with different behavior, depending on the class.
     *
     * @param statement for add Data.
     * @throws SQLException if a database access error occurs.
     */
    public void addData (PreparedStatement statement) throws SQLException;

    /**
     * This method fulfills the function of modifying data to the database.
     *
     * Each class that implements this interface will
     * override this method to perform data modification
     * with different behavior, depending on the class.
     *
     * @param statement for set Data.
     * @throws SQLException if a database access error occurs.
     */
    public void setData(PreparedStatement statement ) throws  SQLException;

    /**
     * This method fulfills the function of removing data from the database.
     *
     * Each class that implements this interface will
     * override this method to perform data removal
     * with different behavior, depending on the class.
     *
     * @param statement for delete Data.
     * @throws SQLException if a database access error occurs.
     */
    public void deleteData (PreparedStatement statement) throws SQLException;

}