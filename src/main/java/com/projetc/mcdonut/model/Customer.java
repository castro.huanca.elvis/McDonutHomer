package com.projetc.mcdonut.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import com.projetc.mcdonut.model.database.QueryExecutor;

/**
 * This class references the client that is accessing our application.
 *
 * This class is responsible for defining the behavior that the
 * queries will have for the database.
 *
 * @see QueryExecutable
 * @see QueryExecutor
 */
public class Customer implements QueryExecutable {

    public static ArrayList<Object> customers = new ArrayList<>();

    /*{
        QueryExecutor.getObjectFromDataBase(Customer.SQL_SELECT_CUSTOMER,new Customer());
    }*/

    public static final String SQL_SELECT_CUSTOMER = "SELECT id_client, nit_client, full_name, date_of_birth, password_customer, email FROM Customer";
    public static final String SQL_INSERT_CUSTOMER = "INSERT INTO Customer(nit_client, full_name, date_of_birth, password_customer, email) VALUES (?, ?, ?, ?, ?)";
    public static final String SQL_UPDATE_CUSTOMER = "UPDATE Customer SET nit_client = ?, full_name = ?, date_of_birth = ?, password_customer = ?, email = ? WHERE id_client = ?";
    public static final String SQL_DELETE_CUSTOMER = "DELETE FROM Customer WHERE id_client = ?";

    private String fullName, password, email;
    private int idCustomer, nit;
    private Date dateOfBirth;

    /**
     * This is a customer with no parameters.
     */
    public Customer() {}

    /**
     * This is a constructor method with all the
     * attributes of the class as parameters.
     *
     * @param fullName customer's full name.
     * @param password customer's password.
     * @param email customer's email.
     * @param dateOfBirth customer's date of birth.
     * @param idCustomer customer's id.
     * @param nit customer's nit.
     */
    public Customer(String fullName, String password, String email, Date dateOfBirth, int nit, int idCustomer) {
        this.fullName = fullName;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.nit = nit;
        this.idCustomer = idCustomer;
    }

    /**
     * This is a constructor method with all the attributes
     * of the class as parameters, except customer's id.
     *
     * @param fullName customer's full name.
     * @param password customer's password.
     * @param email customer's email.
     * @param dateOfBirth customer's date of birth.
     * @param nit customer's nit.
     */
    public Customer(String fullName, String password, String email, Date dateOfBirth, int nit) {
        this.fullName = fullName;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.nit = nit;
    }

    /**
     * This is a constructor method with the
     * customer's id as the only parameter.
     *
     * @param idCustomer customer's id.
     */
    public Customer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    /**
     * This is a constructor method with email and password as the only parameters
     *
     * @param password customer's password.
     * @param email customer's email.
     */
    public Customer(String password, String email) {
        this.password = password;
        this.email = email;
    }

    /**
     * This method gets customer's full name.
     *
     * @return customer's full name.
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * This method sets customer's full name.
     *
     * @param fullName new customer's full name.
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * This method gets customer's password.
     *
     * @return customer's full name.
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method sets customer's password.
     *
     * @param password new customer's password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * This method gets customer's email.
     *
     * @return customer's email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * This method sets customer's email.
     *
     * @param email new customer's email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This method gets customer's date of birth.
     *
     * @return customer's date of birth.
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * This method sets customer's date of birth.
     *
     * @param dateOfBirth new customer's date of birth.
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * This method gets customer's nit.
     *
     * @return customer's nit.
     */
    public int getNit() {
        return nit;
    }

    /**
     * This method sets customer's nit.
     *
     * @param nit new customer's nit.
     */
    public void setNit(int nit) {
        this.nit = nit;
    }

    /**
     * This method gets customer's id.
     *
     * @return customer's id.
     */
    public int getIdCustomer() {
        return idCustomer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return getPassword().equals(customer.getPassword()) && getEmail().equals(customer.getEmail());
    }


    /**
     * Override the hashcode method, if two objects are equal
     * (comparison in the equal method) they refer to the same memory space
     * @return object int type
     */
    @Override
    public int hashCode() {
        return Objects.hash(getPassword(), getEmail());
    }

    /**
     * This method gets data from the Customer table
     * in the database, to add Customers to a list of customers.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param result for get data.
     * @return arraylist of customers obtained from the database.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public ArrayList<Object> getData(ResultSet result) throws SQLException {
        while (result.next()) {
            int idCustomer = result.getInt("id_client");
            int nitCustomer = result.getInt("nit_client");
            String fullName = result.getString("full_name");
            Date dateOfBirth = result.getDate("date_of_birth");
            String password_customer = result.getString("password_customer");
            String email = result.getString("email");

            customers.add(new Customer(fullName, password_customer, email, dateOfBirth, idCustomer, nitCustomer));
        }
        return customers;
    }

    /**
     * This method adds data from the Customer
     * table in the database, to update its records.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for add Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void addData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getNit());
        statement.setString(2, this.getFullName());
        long timeInMilliSeconds = this.getDateOfBirth().getTime();
        statement.setDate(3, new java.sql.Date(timeInMilliSeconds));
        statement.setString(4, this.getPassword());
        statement.setString(5, this.getEmail());
    }

    /**
     * This method updates data from the Customer table in the database.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for set Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void setData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getNit());
        statement.setString(2, this.getFullName());
        long timeInMilliSeconds = this.getDateOfBirth().getTime();
        statement.setDate(3, new java.sql.Date(timeInMilliSeconds));
        statement.setString(4, this.getPassword());
        statement.setString(5, this.getEmail());
        statement.setInt(6, this.getIdCustomer());
    }

    /**
     * This method deletes data from the Customer table in the database.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for delete Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void deleteData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getIdCustomer());
    }

}
