package com.projetc.mcdonut.model.database;

import com.projetc.mcdonut.model.Query;
import com.projetc.mcdonut.model.QueryExecutable;


import java.sql.*;
import java.util.ArrayList;

/**
 * This class is responsible for executing database queries.
 *
 * @see QueryExecutable
 * @see Query
 */
public class QueryExecutor {

    private static Connection connection;
    private static PreparedStatement statement;
    private static ResultSet resultSet;

    /**
     * This is the constructor method.
     *
     * It does not receive parameters, but it is in charge
     * of instantiating the objects necessary for the execution of queries.
     */
    public QueryExecutor(){
        connection = null;
        statement = null;
        resultSet = null;
    }

    /**
     * This method obtains information from the
     * database to save in the form of objects.
     *
     * If we want to use with new types of models,
     * those classes must implement interface QueryExecutable
     *
     * @param sql query that we are going to execute in the database.
     * @param object object of type QueryExecutable to be able to execute
     *               the query with the independent behavior of each type of class.
     * @return array with list of objects obtained from database data.
     */
    public static ArrayList<Object> getObjectFromDataBase ( String sql, QueryExecutable object ){

        ArrayList <Object> objects = new ArrayList<>();

        try {
            connection = DataBaseConnection.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            objects = object.getData(resultSet);

        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {
            closeConnections(resultSet, statement, connection);
        }
        return objects;
    }

    /**
     * This method chooses which method will be executed depending
     * on the name of the query (of the Enum class) that is introduced.
     *
     * @param query Enum class object that helps us choose the right method.
     * @param object object of type QueryExecutable from which we use its different methods.
     * @throws SQLException if a database access error occurs.
     */
    private static void selectUpdateType(Query query, QueryExecutable object) throws SQLException {
        switch (query){
            case INSERT : object.addData(statement);break;
            case UPDATE : object.setData(statement);break;
            case DELETE : object.deleteData(statement);
        }
    }

    /**
     * This method updates the database records.
     * Depending on the arguments passed to the
     * method, the method will know which table to update.
     *
     * @param sql query to be executed.
     * @param object object of type QueryExecutable from which the
     *               appropriate method for updating records can be chosen.
     * @param typeExecution from the enum Query class.
     * @return number of registers updated.
     */
    public static int updateRegisters(String sql, QueryExecutable object, Query typeExecution) {

        int registers = 0;

        try {
            connection = DataBaseConnection.getConnection();
            statement = connection.prepareStatement(sql);

            selectUpdateType(typeExecution, object);

            registers = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {
            closeConnections(statement, connection);
        }
        return registers;
    }

    /**
     * Method to close all connections to the database.
     *
     * @param resultSet to close it.
     * @param preparedStatement to close it.
     * @param connection to close it.
     */
    private static void closeConnections(ResultSet resultSet, PreparedStatement preparedStatement, Connection connection){
        try {
            DataBaseConnection.close(resultSet);
            DataBaseConnection.close(preparedStatement);
            DataBaseConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to close all connections to the database.
     *
     * @param preparedStatement to close it.
     * @param connection to close it.
     */
    private static void closeConnections(PreparedStatement preparedStatement, Connection connection){
        try {
            DataBaseConnection.close(preparedStatement);
            DataBaseConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

