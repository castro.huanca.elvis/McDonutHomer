package com.projetc.mcdonut.model.database;
import java.sql.*;

/**
 * This class establishes a connection to the database.
 *
 * It has the necessary attributes and methods to establish
 * the connection with the schema created in SQL.
 */
public class DataBaseConnection {

    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/dbMcDonut";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASSWORD = "123456";

    /**
     * This method establishes the connection with the database schema.
     *
     * @return the connection in an object of type Connection.
     * @throws SQLException if a database access error occurs.
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
    }

    /**
     * This method releases this ResultSet object's database and JDBC resources
     * immediately instead of waiting for this to happen when it is automatically closed.
     *
     * @param resultSet parameter of type ResultSet that we use to close it.
     * @throws SQLException if a database access error occurs.
     */
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }

    /**
     * This method releases this ResultSet object's database and JDBC resources
     * immediately instead of waiting for this to happen when it is automatically closed.
     *
     * @param statement parameter of type PreparedStatement that we use to close it
     * @throws SQLException if a database access error occurs.
     */
    public static void close(PreparedStatement statement) throws SQLException {
        statement.close();
    }

    /**
     * This method releases this ResultSet object's database and JDBC resources
     * immediately instead of waiting for this to happen when it is automatically closed.
     *
     * @param connection parameter of type Connection that we use to close it
     * @throws SQLException if a database access error occurs.
     */
    public static void close(Connection connection) throws SQLException {
        connection.close();
    }


}
