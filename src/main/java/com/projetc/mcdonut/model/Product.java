package com.projetc.mcdonut.model;

import com.projetc.mcdonut.model.database.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * This class refers to the products that the donut business has available.
 *
 * This class is responsible for defining the behavior that the
 * queries will have for the database.
 *
 * @see QueryExecutable
 * @see QueryExecutor
 */
public class Product implements QueryExecutable {
    public static ArrayList<Object> products = new ArrayList<>();

    static {
        QueryExecutor.getObjectFromDataBase(Product.SQL_SELECT_PRODUCT, new Product());
    }

    public static final String SQL_SELECT_PRODUCT = "SELECT id_product, details, title, type_product, price FROM Product";
    public static final String SQL_INSERT_PRODUCT = "INSERT INTO Product(details, title, type_product, price) VALUES (?, ?, ?, ?)";
    public static final String SQL_UPDATE_PRODUCT = "UPDATE Product SET details = ?, title = ?, type_product = ?, price = ? WHERE id_product = ?";
    public static final String SQL_DELETE_PRODUCT = "DELETE FROM Product WHERE id_product = ?";

    private int idProduct, productPrice, quantity, partialPrice;
    private String details, title, type;

    /**+
     * This is the Products constructor method
     *
     * This constructor method does not have any
     * parameters
     */
    public Product(){
        this.price = 5;
    }

    /**
     * This is the Products constructor method
     *
     * This constructor method has only one parameter
     * and it is the title of the product
     * @param title
     */
    public Product(String title) {
        Product productFound = searchProduct(title, products);
        if (productFound != null) {
            this.title = productFound.getTitle();
            this.details = productFound.getDetails();
            this.idProduct = productFound.getIdProduct();
            this.productPrice = productFound.getProductPrice();
            this.type = productFound.getType();
            this.quantity = 1;
            this.partialPrice = this.getProductPrice() * this.getQuantity();
        }
    }

    public void getProductFromDataBase(String productTitle){
        Product productFound = searchProduct(productTitle, products);
        this.title = productFound.getTitle();
        this.details = productFound.getDetails();
        this.idProduct = productFound.getIdProduct();
        this.productPrice = productFound.getProductPrice();
        this.type = productFound.getType();
    }
    public void changePriceBySum(int num){
        int newPrice = getPrice() + num;
        setPrice(newPrice);
    }
    /**
     * This is a constructor method with the
     * product's id as the only parameter.
     * What it does is search for a product in the database,
     * when it finds one with the same idProduct, the
     * class attributes are updated to that of that object.
     *
     * @param idProduct used to compare with other objects.
     */
    public Product(int idProduct) {
        Product productFound = searchProduct(idProduct, products);
        if (productFound != null){
            this.title = productFound.getTitle();
            this.details = productFound.getDetails();
            this.idProduct = productFound.getIdProduct();
            this.productPrice = productFound.getProductPrice();
            this.type = productFound.getType();
            this.quantity = 1;
            this.partialPrice = this.getProductPrice() * this.getQuantity();

        }
    }

    /**
     * This is a constructor method with all the
     * attributes of the class as parameters.
     *
     * @param idProduct product's id.
     * @param details product's details.
     * @param title product's title.
     * @param type product's type.
     * @param price product's price.
     */
    public Product(String details, String title, String type, int price, int idProduct) {
        this.details = details;
        this.title = title;
        this.type = type;
        this.productPrice = price;
        this.idProduct = idProduct;
    }

    /**
     * This is a constructor method with all the attributes
     * of the class as parameters, except product's id.
     *
     * @param details product's details.
     * @param title product's title.
     * @param type product's type.
     * @param price product's price.
     */
    public Product(String details, String title, String type, int price) {
        this.details = details;
        this.title = title;
        this.type = type;
        this.productPrice = price;
    }

    /**
     * This is the changePriceBySum method
     *
     * It adds a number to the products price
     * @param num type int
     */
    public Product searchProduct(String titleProduct, ArrayList<Object> products){
        Product productReturned = null;
        for (Object productIterator : products) {
            if(((Product) productIterator).getTitle().equals(titleProduct)){
                productReturned = (Product) productIterator;
            }
        }
        return productReturned;
    }







    /**
     * This method searches a product in a product list.
     *
     * This method receives the title of the product and
     * a list of products. Then it returns the product of the list
     * that matches the given id.
     *
     * @param idProduct for compare.
     * @param products arrayList of products.
     * @return arrayList of products.
     */
    private Product searchProduct(int idProduct, ArrayList<Object> products){
        Product productReturned = null;
        for (Object productIterator : products) {
            if(((Product) productIterator).getIdProduct() == idProduct){
                productReturned = (Product) productIterator;
            }
        }
        return productReturned;
    }

    /**
     * This method gets data from the Product table
     * in the database, to add Products to a list of customers.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param result for get data.
     * @return arraylist of products obtained from the database.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public ArrayList<Object> getData(ResultSet result) throws SQLException {

        while ( result.next() ) {
            int idProduct = result.getInt("id_product");
            String details = result.getString("details");
            String title = result.getString("title");
            String typeProduct = result.getString("type_product");
            int price = result.getInt("price");

            products.add(new Product(details, title, typeProduct, price, idProduct));
        }
        return products;
    }

    /**
     * This method adds Data from the Product
     * table in the database, to update its records.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for add Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void addData(PreparedStatement statement) throws SQLException {
        statement.setString(1, this.getDetails());
        statement.setString(2, this.getTitle());
        statement.setString(3, this.getType());
        statement.setInt(4, this.getProductPrice());
    }

    /**
     * This method updates data from the Product table in the database.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for set Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void setData(PreparedStatement statement) throws SQLException {
        statement.setString(1, this.getDetails());
        statement.setString(2, this.getTitle());
        statement.setString(3, this.getType());
        statement.setInt(4, this.getProductPrice());
        statement.setInt(5,this.getIdProduct());
    }

    /**
     * This method deletes data from the Product table in the database.
     *
     * This method is overridden due to the
     * implementation of the QueryExecutable interface.
     *
     * @param statement for delete Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void deleteData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getIdProduct());
    }


    /**
     * This method gets product's id.
     *
     * @return product's id.
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * This method gets product's details.
     *
     * @return product's details.
     */
    public String getDetails() {
        return details;
    }

    /**
     * This method sets product's details.
     *
     * @param details product's details.
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * This method gets product's title.
     *
     * @return product's title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method sets product's title.
     *
     * @param title product's title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * This method gets product's type.
     *
     * @return product's type.
     */
    public String getType() {
        return type;
    }

    /**
     * This method sets product's type.
     *
     * @param type product's type.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * This method gets product's price.
     *
     * @return product's price.
     */
    public int getProductPrice() {
        return productPrice;
    }

    /**
     * This method sets product's price.
     *
     * @param productPrice product's price.
     */
    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * This method sets product's id.
     * @param idProduct product's
     */
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * This method gets product's quantity.
     * @return product's
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * This method sets product's quantity.
     * @param quantity  product's quantity;
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * This method gets product's partialPrice;
     *
     * @return product's.
     */
    public int getPartialPrice() {
        return partialPrice;
    }

    /**
     * This method sets product's partialPrice.
     *
     * @param partialPrice product's partialPrice;
     */
    public void setPartialPrice(int partialPrice) {
        this.partialPrice = partialPrice;
    }
}
