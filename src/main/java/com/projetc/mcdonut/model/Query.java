package com.projetc.mcdonut.model;

/**
 * In this enum class are the types of executable queries in the database.
 */
public enum Query {

    INSERT, DELETE, SELECT, UPDATE

}
