package com.projetc.mcdonut.model;

import java.sql.*;
import java.util.ArrayList;
import com.projetc.mcdonut.model.database.*;

/**
 * This class has the function of
 * facilitating the relationship
 * between tables "Product" and "Order".
 *
 * @see QueryExecutable
 * @see QueryExecutor
 */
public class OrderProduct implements QueryExecutable{

    private int idOrderProduct, idOrder, idProduct;
    private int partialPrice;
    private int quantity;

    public static final String SQL_INSERT_ORDER_PRODUCT = "INSERT INTO Order_Product (id_order, id_product) VALUES (?, ?)";
    public static final String SQL_SELECT_ORDER_PRODUCT = "SELECT id_order_product, id_order, id_product FROM Order_Product";
    public static final String SQL_UPDATE_ORDER_PRODUCT = "UPDATE Order_Product SET id_order = ?, id_product = ?, WHERE id_order_product = ?";
    public static final String SQL_DELETE_ORDER_PRODUCT = "DELETE FROM Order_Product WHERE id_order_product = ?";

    /**
     * This is a constructor method that takes no parameters.
     */
    public OrderProduct(){}

    /**
     * This is a constructor method that receives
     * all attributes as parameters.
     *
     * @param idOrderProduct OrderProduct's id.
     * @param idOrder OrderProduct's idOrder.
     * @param idProduct OrderProduct's idProduct.
     */
    public OrderProduct(int idOrderProduct, int idOrder, int idProduct) {
        this.idOrderProduct = idOrderProduct;
        this.idOrder = idOrder;
        this.idProduct = idProduct;
        this.quantity = 1;
        this.partialPrice = new Product(idProduct).getProductPrice();
    }

    /**
     * This is a constructor method that
     * receives only the idOrder and idProduct as parameters.
     *
     * @param idOrder OrderProduct's idOrder.
     * @param idProduct OrderProduct's idProduct.
     */
    public OrderProduct(int idOrder, int idProduct) {
        this.idOrder = idOrder;
        this.idProduct = idProduct;
        this.quantity = 1;
        this.partialPrice = new Product(idProduct).getProductPrice();
    }

    /**
     * This is a constructor method that
     * receives the product id as its only parameter.
     *
     * @param idProduct OrderProduct's idProduct.
     */
    public OrderProduct (int idProduct){
        this.idProduct = idProduct;
    }

    /**
     * This method gets OrderProduct's idOrderProduct.
     *
     * @return
     */
    public int getIdOrderProduct() {
        return idOrderProduct;
    }

    /**
     * This method sets OrderProduct's idOrderProduct.
     *
     * @param idOrderProduct OrderProduct's idOrderProduct.
     */
    public void setIdOrderProduct(int idOrderProduct) {
        this.idOrderProduct = idOrderProduct;
    }

    /**
     * This method gets OrderProduct's idOrder.
     *
     * @return OrderProduct's idOrder.
     */
    public int getIdOrder() {
        return idOrder;
    }

    /**
     * This method sets OrderProduct's idOrder.
     *
     * @param idOrder OrderProduct's idOrder.
     */
    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    /**
     * This method gets OrderProduct's idProduct.
     *
     * @return OrderProduct's idProduct.
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * This method sets OrderProduct's idProduct.
     *
     * @param idProduct OrderProduct's idProduct.
     */
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * This method gets OrderProduct's partialPrice.
     *
     * @return OrderProduct's partialPrice.
     */
    public int getPartialPrice() {
        return partialPrice;
    }

    /**
     * This method sets OrderProduct's partialPrice.
     *
     * @param partialPrice OrderProduct's partialPrice.
     */
    public void setPartialPrice(int partialPrice) {
        this.partialPrice = partialPrice;
    }

    /**
     * This method gets OrderProduct's quantity.
     *
     * @return OrderProduct's quantity.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * This method sets OrderProduct's quantity.
     *
     * @param quantity OrderProduct's quantity.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * This method fulfills the function of obtaining data from the database.
     * <p>
     * Each class that implements this interface will
     * override this method to execute the data
     * retrieval with different behaviors, depending on the class.
     *
     * @param result for get data.
     * @return Arraylist of objects obtained from data in the database.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public ArrayList<Object> getData(ResultSet result) throws SQLException {
        ArrayList<Object> orders = new ArrayList<>();
        while (result.next()){
            int idOrderProduct = result.getInt("id_order_product");
            int idOrder = result.getInt("id_order");
            int idProduct = result.getInt("id_product");

            orders.add(new OrderProduct(idOrderProduct, idProduct, idOrder));
        }
        return orders;
    }


    /**
     * This method fulfills the function of adding data to the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data aggregation
     * with different behavior, depending on the class.
     *
     * @param statement for add Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void addData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getIdOrder());
        statement.setInt(2, this.getIdProduct());
    }

    /**
     * This method fulfills the function of modifying data to the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data modification
     * with different behavior, depending on the class.
     *
     * @param statement for set Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void setData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getIdOrder());
        statement.setInt(2, this.getIdProduct());
    }


    /**
     * This method fulfills the function of removing data from the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data removal
     * with different behavior, depending on the class.
     *
     * @param statement for delete Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void deleteData(PreparedStatement statement) throws SQLException {
        statement.setInt(1, this.getIdOrderProduct());
    }
}