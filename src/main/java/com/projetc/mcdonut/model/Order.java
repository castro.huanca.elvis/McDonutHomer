package com.projetc.mcdonut.model;

import javafx.scene.control.Alert;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Order implements QueryExecutable{

    private static Order order;

    public static final String SQL_SELECT_ORDER = "SELECT id_order, id_client FROM Order";
    public static final String SQL_INSERT_ORDER = "INSERT INTO Order(id_client) VALUES (?)";
    public static final String SQL_UPDATE_ORDER = "UPDATE Order SET id_client = ?";
    public static final String SQL_DELETE_ORDER = "DELETE FROM Order WHERE id_client";

    private Customer customer;
    private ArrayList <Object> productsSelected;
    private int idOrder;

    /**
     * This is the Order method constructor
     *
     * @param customer the customer
     * @param idOrder the orderId
     */
    public Order(Customer customer, int idOrder) {
        this.customer = customer;
        this.idOrder = idOrder;
    }

    /**
     * This the Order method Constructor
     *
     * @param customer the costumer
     */
    public Order (Customer customer){
        this.customer = customer;
        productsSelected = new ArrayList<>();
    }

    /**
     * This method performs a change on command.
     * @param order the customer's order
     */
    public static void setOrder(Order order) {
        Order.order = order;
    }

    /**
     * This is the Order method Constructor
     */

    public Order(){
        productsSelected = new ArrayList<>();
    }

    /**
     * This is the searchProductOnList method
     *
     * <p>
     *     It looks a product by a give title and ones the
     *     program finds it, it returns it.
     * </p>
     * @param title the title of the product you are searching
     * @return the found product
     */
    public Product searchProductOnList(String title){
        Product productFound = null;
        for (Object object:
                productsSelected) {
            if (((Product) object).getTitle().equals(title)){
                productFound = ((Product) object);
            }
        }
        return productFound;
    }

    

    /**
     * This method returns the product
     *
     * @param i the index of the product
     * @return the product
     */
    public Product returnProduct(int i){
        return (Product) productsSelected.get(i);
    }

    /**
     * This method is the getproductsSelectedSize method
     *
     * @return the size of the productSelected list
     */
    public int getproductsSelectedSize(){
        return productsSelected.size();
    }

    /**
     * This is the getCustomer method
     *
     * @return the costumer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * This is the setCustomer method
     *
     * @param customer the costumer you want to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**This methos is the getProductsSelected method
     *
     * @return the productSelected list
     */
    public ArrayList<Object> getProductsSelected() {
        return productsSelected;
    }

    /**
     * This is the getTitlesOfProductsSelected method
     *
     * <p>It returns a list of only titles of all items
     * in the productSelected list</p>
     * @return a String Array
     */
    public ArrayList<String> getTitlesOfProductsSelected(){
        ArrayList<String> titleProductsSelected = new ArrayList<String>();
        for (Object object:
             productsSelected) {
            titleProductsSelected.add(((Product) object).getTitle());
        }
        return titleProductsSelected;
    }

    /**
     * This is the method productsSelected method
     *
     * @param productsSelected ArrayList<Object>
     */
    public void setProductsSelected(ArrayList<Object> productsSelected) {
        this.productsSelected = productsSelected;
    }

    /**
     * This is the addProduct method
     *
     * @param product Object
     */
    public void addProduct(Object product){
        productsSelected.add(product);
    }

    /**
     * This is the getOrderClient method
     *
     * @return an order
     */
    public static Order getOrderClient (){
        if(order == null){
            order = new Order();
        }
        return order;
    }

    /**
     * This is the verifyIfProductSelectedListIsEmpty method
     *
     * It returns if our productsSelected list is empty and
     * gives us an error message
     *
     * @return boolean type
     */
    public boolean verifyIfProductSelectedListIsEmpty(){
        boolean isEmpty = false;
        if (getproductsSelectedSize() == 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("You must select at least one Donut");
            alert.showAndWait();
            isEmpty = true;
        }
        return isEmpty;
    }


    /**
     * This method fulfills the function of obtaining data from the database.
     * <p>
     * Each class that implements this interface will
     * override this method to execute the data
     * retrieval with different behaviors, depending on the class.
     *
     * @param result for get data.
     * @return Arraylist of objects obtained from data in the database.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public ArrayList<Object> getData(ResultSet result) throws SQLException {

        ArrayList<Object> orders = new ArrayList<>();

        while(result.next()){
            int idOrder = result.getInt("id_order");
            int idCustomer = result.getInt("id_client");

            orders.add(new Order(new Customer(idCustomer), idOrder));

        }
        return orders;
    }

    /**
     * This method fulfills the function of adding data to the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data aggregation
     * with different behavior, depending on the class.
     *
     * @param statement for add Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void addData(PreparedStatement statement) throws SQLException {


    }

    /**
     * This method fulfills the function of modifying data to the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data modification
     * with different behavior, depending on the class.
     *
     * @param statement for set Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void setData(PreparedStatement statement) throws SQLException {

    }

    /**
     * This method fulfills the function of removing data from the database.
     * <p>
     * Each class that implements this interface will
     * override this method to perform data removal
     * with different behavior, depending on the class.
     *
     * @param statement for delete Data.
     * @throws SQLException if a database access error occurs.
     */
    @Override
    public void deleteData(PreparedStatement statement) throws SQLException {

    }
}