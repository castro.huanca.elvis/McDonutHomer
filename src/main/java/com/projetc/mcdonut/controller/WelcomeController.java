package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the welcome controller class.
 *
 * It has its necessary methods and an
 * implementation of Initializable that has
 * the initialize method with its corresponding
 * data.
 */

public class WelcomeController implements Initializable {

    @FXML
    private Button login;
    @FXML
    private Button signUp;

    /**
     * This is the method of changing the scene to SingUp.
     *
     * Is inside a try, catch, in which the try
     * calls the method of the ChangeScene class
     * which is the changeScreenButtonWelcome and
     * with its respective parameters, its signUp
     * event, the FXML, and its title
     * that the method asks and the catch
     * prints exception's type if the action does not pass.
     *
     * @param signUp action event.
     */

    public void changSceneSingUp(ActionEvent signUp) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(signUp, "SignUp.fxml","Sign Up");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the Login scene change method.
     *
     *  Is inside a try, catch, in which the try
     *  calls the ChangeScene class method which
     *  is the changeScreenButtonWelcome and with
     *  its respective parameters, its login event,
     *  the login FXML, and its title
     *  that the method asks and the catch
     *  prints exception's type if the action does not pass.
     *
     * @param login action event.
     */
    public void changSceneLogin(ActionEvent login) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(login, "Login.fxml","Login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * It is a proper Java method that gives the order to initialize.
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }
}
