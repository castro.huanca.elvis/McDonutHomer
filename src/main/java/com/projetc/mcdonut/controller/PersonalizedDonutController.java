package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 *This is the Personalized Donut Controller class.
 *
 * It has an implementation of Initializable, more with
 * some instances and with its methods.
 */

public class PersonalizedDonutController implements Initializable {
    private Product product;
    private Order order;

    /**
     *This method used to change the scene to the Coffee Menu.
     *
     *This allows you to change scenes to view and place your order.
     *
     * @param coffeeMenu action event.
     */

    public void changSceneToCoffeeMenu(ActionEvent coffeeMenu) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(coffeeMenu, "CoffeeMenu.fxml","Coffee Menu");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *This method is used to change the scene of Personalized Donut.
     *
     * @param personalizedDonut action event.
     */

    public void changSceneToPersonalizedDonut(ActionEvent personalizedDonut) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(personalizedDonut, "PersonalizedDonut.fxml","Personalized Donut");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private CheckBox chocolateGlazeButton;

    @FXML
    private CheckBox coconutGlazeButton;

    @FXML
    private CheckBox creamCheeseFrostingButton;

    @FXML
    private CheckBox gratedCoconutButton;

    @FXML
    private CheckBox longColoredSprinklesButton;

    @FXML
    private CheckBox meMsButton;

    @FXML
    private CheckBox oreoCookiesInPiecesButton;

    @FXML
    private CheckBox roundColoredSprinklesButton;

    @FXML
    private CheckBox royalIcingButton;

    @FXML
    private CheckBox strawberryGlazeButton;

    @FXML
    private CheckBox walnutInPiecesButton;

    @FXML
    private CheckBox whippedCreamFrostingButton;

    @FXML
    private CheckBox whiteChocolateGlazeButton;
    @FXML
    private Label labelDetailsPersonalizedDonud;

    @FXML
    private TextArea labelAddedIngredients = new TextArea();


    @FXML
    void setOreoCookiesInPiecesDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Oreo Cookies in Pieces").getDetails());
    }

    @FXML
    void setGratedCoconutDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Grated Coconut").getDetails());
    }

    @FXML
    void setLongColoredSprinklesDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Long Colored Sprinkles").getDetails());
    }

    @FXML
    void setMeMsDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("MEMs").getDetails());
    }

    @FXML
    void setWalnutInPiecesDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Walnut in Pieces").getDetails());
    }

    @FXML
    void setRoundColoredSprinklesDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Round Colored Sprinkles").getDetails());
    }

    @FXML
    void setChocolateGlazeDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Chocolate Glaze").getDetails());
    }

    @FXML
    void setCreamCheeseFrostingDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Cream Cheese Frosting").getDetails());
    }

    @FXML
    void setConutGlazeDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Coconut Glaze").getDetails());
    }

    @FXML
    void setRoyalIcingDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Royal Icing").getDetails());
    }

    @FXML
    void setWhiteChocolateGlazeDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("White Chocolate Glaze").getDetails());
    }

    @FXML
    void setWhippedCreamFrostingDetaiOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Whipped Cream Frosting").getDetails());
    }

    @FXML
    void setstrawberryGlazeDetailsOnLabel(ActionEvent event) {
        labelDetailsPersonalizedDonud.setText(new Product("Strawberry Glaze").getDetails());
    }

    ArrayList<String> ingredientsNamesList = new ArrayList<>();

    @FXML
    void obtainChosenIngredients(ActionEvent event) {


        if (oreoCookiesInPiecesButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("Oreo Cookies");
        }
        if (gratedCoconutButton.isSelected()){
            product.changePriceBySum(3);
            ingredientsNamesList.add("gratedCoconutButton");

        }
        if (longColoredSprinklesButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("longColoredSprinklesButton");
        }
        if (meMsButton.isSelected()){
            product.changePriceBySum(4);
            ingredientsNamesList.add("meMsButton");
        }
        if (walnutInPiecesButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("walnutInPiecesButton");

        }
        if (roundColoredSprinklesButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("roundColoredSprinklesButton");
        }
        if (chocolateGlazeButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("chocolateGlazeButton");
        }
        if (creamCheeseFrostingButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("creamCheeseFrostingButton");
        }
        if (coconutGlazeButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("coconutGlazeButton");
        }
        if (royalIcingButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("royalIcingButton");
        }
        if (whiteChocolateGlazeButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("whiteChocolateGlazeButton");

        }
        if (whippedCreamFrostingButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("whippedCreamFrostingButton");
        }
        if (strawberryGlazeButton.isSelected()){
            product.changePriceBySum(2);
            ingredientsNamesList.add("strawberryGlazeButton");
        }

        order.addProduct(product);
        changeSceneToConfirmation(event);

    }

    public String changeArrayToString(){
        String itemsTitles = "";
        for (String title: ingredientsNamesList) {
            itemsTitles+= title +" --- ";
        }
        return itemsTitles;
    }

    public void setLabelFinal(ActionEvent event){
        labelAddedIngredients.setText(changeArrayToString());
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = Order.getOrderClient();
        product = new Product();
        product.setPrice(6);
        product.setTitle("Personalizated Donut");

    }

    @FXML
    private void changeSceneToConfirmation(ActionEvent event){
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(event, "PersonalizedDonutConfirmation.fxml", "Personalized Donut Confirmation");
            setLabelFinal(event);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
