package com.projetc.mcdonut.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * This class is the WaitTimeController class
 *
 * <p>
 *     This classs implements Initializable
 * </p>
 */
public class WaitTimeController implements Initializable {
    @FXML
    Label timer;
    Random random = new Random();

    private int second = random.nextInt(50);

    public static final int Valorminimo =0;

    /**
     *the seconds are decreasing with random number.
     */
    public void oneSecondPassed(){
        while(second>Valorminimo) {
            second--;
            timer.setText(Integer.toString(second));
            if (second == Valorminimo) {
                timer.setText("Thanks for buying");
            }
        }
    }

    /**
     *
     * We initialize oneSecondPassed
     * @param url represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        oneSecondPassed();
    }


}
