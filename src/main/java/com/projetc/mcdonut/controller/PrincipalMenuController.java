package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the Principal Menu Controller class.
 *
 * It has its necessary methods and an
 * implementation of Initializable that has
 * the initialize method with its corresponding
 * data.
 */
public class PrincipalMenuController implements Initializable {

    /**
     * This is the changeSceneToDonutMenu method.
     *
     *<p>
     *    Is inside a try, catch, in which the try calls
     *    the ChangeScene class method which is the
     *    changeScreenButtonPushed and with its respective
     *    parameters, its donutMenu event, the DonutMenu FXML,
     *    and its title that the method asks and the catch
     *    prints exception's type if the action does not pass.
     *</p>
     *
     * @param donutMenu action event.
     */
    public void changeSceneToDonutMenu(ActionEvent donutMenu) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(donutMenu, "DonutMenu.fxml","Donut Menu");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the changeSceneToDonutMenu method.
     *
     *<p>
     *     Is inside a try, catch, in which the try calls the
     *     ChangeScene class method which is the changeScreenButtonPushed
     *     and with its respective parameters, its combo
     *     event, the Combo FXML , and its title that the
     *     method asks and the catch prints exceptions type
     *     if the action does not pass.
     *</p>
     *
     * @param combo action event.
     */
    public void changeSceneToCombo(ActionEvent combo) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(combo, "Combo.fxml","Combos");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the changeSceneToPersonalizedDonuts method.
     *
     * <p>
     *     Is inside a try, catch, in which the try calls the
     *     ChangeScene class method which is the changeScreenButtonPushed
     *     and with its respective parameters, its personalizedDonuts
     *     event, the PersonalizedDonuts FXML , and its title that
     *     the method asks and the catch prints exceptions type if
     *     the action does not pass.
     * </p>
     *
     * @param personalizedDonuts action event.
     */
    public void changeSceneToPersonalizedDonuts(ActionEvent personalizedDonuts) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(personalizedDonuts, "PersonalizedDonuts.fxml","Personalized Donuts");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * It is a proper Java method that gives the order to initialize.
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
