package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
/***
 * This is the DonutMenuController class
 *
 * <p>
 *     This class is the controller of the fxml file
 *     DonutsMenu.It obtains the donuts chosen by the
 *     user and also shows its details
 * </p>
 * @see Order
 * @author Elviscocho
 */
public class DonutMenuController implements Initializable {
    private Order order;
    @FXML private CheckBox homer_donut_checkbox;
    @FXML private CheckBox chocolate_donut_checkbox;
    @FXML private CheckBox sugar_donut_checkbox;
    @FXML private CheckBox white_choc_donut_checkbox;
    @FXML private CheckBox vanilla_donut_checkbox;
    @FXML private CheckBox oreo_donut_checkbox;
    @FXML private CheckBox donate_normal_icing_checkbox;
    @FXML private Label labelDonutDetails;

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Chocolate Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setChocolateDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Chocolate Donut").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Sugar Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setSugarDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Sugar Donut").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     White Chocolate Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setWhiteChocDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("White Chocolate Donut").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Vanilla Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setVanillaDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Vanilla Donut").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Oreo Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setOreoDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Oreo Donut").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Donate Vegan Icing specific details
     * </p>
     * @param event action event
     */
    @FXML void setDonateVeganIcingDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Donate with Vegan Icing").getDetails());}

    /***
     * This method changes the DonutDetailLabel
     *
     * <p>
     *     It sets the labelDonutDetails label to the
     *     Homero Donut specific details
     * </p>
     * @param event action event
     */
    @FXML void setHomerDonutDetailsOnLabel(ActionEvent event){labelDonutDetails.setText(new Product("Homero Donut").getDetails());}

    /***
     * This method changes a scene
     *
     * <p>This method changes specifically to the ExtraIngredients
     * scene which is located in the file ExtraIngredients.fxml</p>
     * @param extras action event
     */
    @FXML
    public void changeSceneExtraIngredients(ActionEvent extras){
        if (order.verifyIfProductSelectedListIsEmpty()){
            try {
                ChangeScene.getChangeScene().changeScreenButtonPushed(extras, "DonutMenu.fxml","Donuts Menu");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                ChangeScene.getChangeScene().changeScreenButtonPushed(extras, "ExtraIngredientsMenu.fxml","Extre Ingredients");
            } catch (IOException e) {
                e.printStackTrace();
            }}

    }

    /***
     * This method obtains chosen donuts by the user
     * <p>
     *     It obtains all the donuts selected by
     *     the user and saves them in the list
     *     productsSelected in the class Order
     * </p>
     * @param event action event
     */
    @FXML void obtainChosenDonuts(ActionEvent event) {
        if (homer_donut_checkbox.isSelected()){order.addProduct(new Product("Homero Donut"));}
        if (chocolate_donut_checkbox.isSelected()){order.addProduct(new Product("Chocolate Donut"));}
        if (sugar_donut_checkbox.isSelected()){order.addProduct(new Product("Sugar Donut"));}
        if (white_choc_donut_checkbox.isSelected()){order.addProduct(new Product("White Chocolate Donut"));}
        if (vanilla_donut_checkbox.isSelected()){order.addProduct(new Product("Vanilla Donut"));}
        if (oreo_donut_checkbox.isSelected()){order.addProduct(new Product("Oreo Donut"));}
        if (donate_normal_icing_checkbox.isSelected()){order.addProduct(new Product("Donate with Vegan Icing"));}
        changeSceneExtraIngredients(event);
        for (int i = 0; i < order.getproductsSelectedSize(); i++) {
            System.out.println(((Product) order.getProductsSelected().get(i)).getTitle());
        }
    }
    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * Here we initialize the order to the clientsOrderList
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects. This helps us when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = Order.getOrderClient();;
    }
}
