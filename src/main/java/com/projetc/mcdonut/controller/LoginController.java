package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Customer;
import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the class of Login Controller
 *
 *  It has its corresponding methods
 *  implementing 'Initializable'
 *  in order to have its initialization method
 */

public class LoginController implements Initializable {
    @FXML
    private Button login;
    @FXML
    private Button signUp;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private Button welcome;

    private Order order;

    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects.
     */

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    /**
     * This is the method "EveryKey" so that the user DOES NOT ENTER ANYTHING IN HIS DATA
     * <p>
     *  It helps us that no TextField is empty for this we need two parameters.
     *  This contains the comparison of the entered text if it is nothing,
     *  it should not continue to the next scene
     * </p>
     *
     * @param event referring to the TextField of the SceneBuilder.
     * @param textField refers to the text field (code)
     */
    @FXML
    private void eventKey(KeyEvent event, TextField textField){
        Object eventText = event.getSource();
        if (eventText.equals(textField)){
            if (event.getCharacter().equals(" ")){
                event.consume();
            }
        }
    }

    /**
     * This method is specifically for the user's email so do not try to avoid it.
     *
     * The EventKey has an event for the text to enter on the sceneBuilder screen, and the text to compare.
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyEmailField (KeyEvent keyEvent){
        eventKey(keyEvent, email);
    }

    /**
     * This method is specifically for the user's password so do not try to avoid it.
     *
     *  The EventKey has an event for the text to enter on the sceneBuilder screen, and the text to compare.
     *
     * @param keyEvent referring to the PasswordField of the SceneBuilder.
     */
    @FXML
    private void eventKeyPasswordField (KeyEvent keyEvent){
        eventKey(keyEvent, password);
    }

    /**
     * This is the method to change the scene to "Sign Up".
     *
     * <p>This method helps us to change the scene, specifically to "Sign In",
     * we implement the 'Singleton' so we don't instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title you want for the next scene, that parameter is from the changeScreenButtonPushed method.
     *
     * This method will only need the event action for changeScene.</p>
     *
     * @param signUp action event of the SceneBuilder.
     */

    public void changeSceneSignUp(ActionEvent signUp) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(signUp, "SignUp.fxml","Sign Up");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the method to change the scene to "welcome".
     *
     * <p>This method helps us to change the scene, specifically to "welcome",
     * we implement the 'Singleton' so we don't instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title you want for the next scene, that parameter is from the changeScreenButtonPushed method.
     *
     * This method will only need the event action for changeScene.</p>
     *
     * @param welcome is the Action where the event will go.
     */
    public void changeSceneToWelcome(ActionEvent welcome) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(welcome, "Welcome.fxml","Welcome");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the method to change the scene to "Main".
     *
     * <p>This method helps us to change the scene, specifically to "Main",
     * we implement the 'Singleton' so we don't instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title you want for the next scene, that parameter is from the changeScreenButtonPushed method.
     *
     * This method will only need the event action for changeScene.</p>
     *
     * @param principalManu is the Action where the event will go.
     */
    public void changeScenePrincipalMain(ActionEvent principalManu) throws IOException {
        Customer customer = new Customer(email.getText(), password.getText());
        order = Order.getOrderClient();
        if(customer.getFullName() != null){
            order.setCustomer(customer);
            ChangeScene.getChangeScene().changeScreenButtonPushed(principalManu, "PrincipalMenu.fxml","Main Mc-Donut");
        }
        else{
            ChangeScene.getChangeScene().changeScreenButtonPushed(principalManu, "SignUp.fxml", "Sign Up");

        }
    }
}
