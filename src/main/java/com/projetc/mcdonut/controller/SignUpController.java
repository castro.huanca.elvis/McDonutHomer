package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Customer;
import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Query;
import com.projetc.mcdonut.model.database.QueryExecutor;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

/**
 *This is the class of Sign In Controller
 *
 * It has its corresponding methods
 * implementing 'Initializable'
 * in order to have its initialization method.
 */

public class SignUpController implements Initializable{
    Order order;
    @FXML
    private Button signUp;
    @FXML
    private Button welcome;
    @FXML
    private TextField fullName;
    @FXML
    private TextField email;
    @FXML
    private TextField nit;
    @FXML
    private TextField dateOfBirth;
    @FXML
    private PasswordField password;
    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * The user's nit has to be only numbers, that's why we have to initialize it to "Initialize".
     *
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nit.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getText().matches("\\d+") || change.getText().equals("")) {
                return change;
            } else {
                change.setText("");
                change.setRange(
                        change.getRangeStart(),
                        change.getRangeStart()
                );
                return change;
            }
        }));
    }

    /**
     * This is the method "EveryKey" so that the user DOES NOT ENTER ANYTHING IN HIS DATA
     * <p>
     *    It helps us that no TextField is empty for this we need two parameters.
     *   This contains the comparison of the entered text if it is nothing,
     *   it should not continue to the next scene
     * </p>
     *
     * @param event referring to the TextField of the SceneBuilder.
     * @param textField refers to the text field (code)
     */
    @FXML
    private void eventKey(KeyEvent event, TextField textField){
        Object eventText = event.getSource();
        if (eventText.equals(textField)){
            if (event.getCharacter().equals(" ")){
                event.consume();
            }
        }
    }

    /**
     * This method is specifically for the user's email so do not try to avoid it.
     *
     * The EventKey has an event for the text to enter on the sceneBuilder screen, and the text to compare.
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyEmail (KeyEvent keyEvent){
        eventKey(keyEvent, email);
    }

    /**
     *This method is specifically for client name
     *
     * <p>This prevents the user from not entering anything in their text field,
     * With this method we prevent them from omitting the full name.</p>
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyFullName (KeyEvent keyEvent){
        eventKey(keyEvent, fullName);
    }

    /**
     *This method is specifically for the NIT or C.I. the client's
     *
     * <p>This prevents the user from entering anything in their text field,
     * With this method we prevent them from omitting the Nit or C.I.</p>
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyNIT (KeyEvent keyEvent){
        eventKey(keyEvent, nit);
    }

    /**
     *This method is specifically for the client password
     *
     * This prevents the user from entering anything in their text field,
     * With this method we prevent them from omitting the password
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyPasswordField (KeyEvent keyEvent){
        eventKey(keyEvent, password);
    }

    /**
     *This method is specifically for the client's birthday
     *
     * This prevents the user from entering anything in their text field,
     * With this method we prevent them from omitting the date of birth.
     *
     * @param keyEvent referring to the TextField of the SceneBuilder.
     */
    @FXML
    private void eventKeyDateOfBirth (KeyEvent keyEvent){
        eventKey(keyEvent, dateOfBirth);
    }

    /**
     *This is the method to change the scene welcome.
     *
     *<p>This method helps us to change the scene, specifically Welcome,
     * we implement the 'Singleton ' to not instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title that you would like for the next scene.</p>
     *
     * @param welcome action event.
     */
    public void changeSceneToWelcome(ActionEvent welcome) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(welcome, "Welcome.fxml","Welcome");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * This method is to add new users or clients in the database.
     *
     * <p>This new user be added to the database with their respective data,
     * once added you can continue with the application.
     * In case your password and gmail coincide with the database, the user will enter login</p>
     *
     * @param event the event action to start the new user.
     */

    public void addCustomer(ActionEvent event) throws IOException {
        Date dateOfBirthInFormatDate = changeStringToDate(dateOfBirth.getText());
        order = Order.getOrderClient();
        Customer customer = new Customer(email.getText(), password.getText());
        if(customer.getFullName() == null){

            customer = new Customer(fullName.getText(), password.getText(),
                    email.getText(), dateOfBirthInFormatDate, Integer.parseInt(nit.getText()));

            QueryExecutor.updateRegisters(Customer.SQL_INSERT_CUSTOMER, customer, Query.INSERT);
            changeScenePrincipalMenu(event);

            order.setCustomer(customer);
        }
        else{
            ChangeScene.getChangeScene().changeScreenButtonPushed(event, "Login.fxml", "Login");
        }
    }

    /**
     * This is the method to change the scene to "Principal Manu".
     *
     * <p>This method helps us to change the scene, specifically Principal Manu,
     * we implement the 'Singleton ' to not instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title that you would like for the next scene.
     * In case of an exception it is in a try catch</p>
     *
     * @param principalMenu The event action is to go to the new scene (SceneBuilder).
     */
    private void changeScenePrincipalMenu(ActionEvent principalMenu) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(principalMenu, "PrincipalMenu.fxml","Principal Menu Mc-Donut");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *This method is to change from a String to Date
     *
     * we get to separate by year, month and day to return it as a date.
     *
     * @param date we receive the data in string.
     * @return dateOfBirth the client's
     */
    public Date changeStringToDate(String date){
        String[] listOfDate = date.split("-");
        Date dateOfBirth = new Date(Integer.parseInt(listOfDate[0])-1900,Integer.parseInt(listOfDate[1]), Integer.parseInt(listOfDate[2]));
        return dateOfBirth;
    }

}
