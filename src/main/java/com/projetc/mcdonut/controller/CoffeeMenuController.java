package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/***
 * This is the CoffeeMenuController class
 *
 * <p>
 *     This class is the controller of the fxml file
 *     CoffeeMenu.It obtains the coffees chosen by the
 *     user and also shows its details
 * </p>
 * @see Order
 * @author Elviscocho
 */
public class CoffeeMenuController implements Initializable {
    private Order order;

    @FXML private CheckBox americano_drink;
    @FXML private CheckBox cocoa_drink;
    @FXML private Label labelCoffeeDetails;
    @FXML private CheckBox espresso_drink;
    @FXML private CheckBox frappe_drink;
    @FXML private CheckBox irish_drink;
    @FXML private CheckBox latte_drink;
    @FXML private CheckBox mocha_drink;

    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * Here we initialize the order to the clientsOrderList
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects. This helps us when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = Order.getOrderClient();
    }

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Americano Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setAmericanoDetailsOnLabel(ActionEvent event) {labelCoffeeDetails.setText(new Product("Americano").getDetails());}

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Cocoa Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setCocoaDetailsOnLabel(ActionEvent event) {
        labelCoffeeDetails.setText(new Product("Cocoa").getDetails());
    }

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Espresso Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setEspressoDetailsOnLabel(ActionEvent event) {labelCoffeeDetails.setText(new Product("Espresso").getDetails());}

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Frappe Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setFrappeDetailsOnLabel(ActionEvent event) {
        labelCoffeeDetails.setText(new Product("Frappe").getDetails());
    }

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Irish Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setIrishDetailsOnLabel(ActionEvent event) {
        labelCoffeeDetails.setText(new Product("Irish").getDetails());
    }

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Latte Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setLatteDetailsOnLabel(ActionEvent event) {
        labelCoffeeDetails.setText(new Product("Latte").getDetails());
    }

    /***
     * This method changes the CoffeeDetailLabel
     *
     * <p>
     *     It sets the Coffee Details label to the
     *     Mocha Coffee specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setMochaDetailsOnLabel(ActionEvent event) {
        labelCoffeeDetails.setText(new Product("Mocha").getDetails());
    }

    /***
     * This method changes a scene
     *
     * <p>This method changes specifically to the ExtraProductsMenu
     * scene which is located in the file ExtrasMenu.fxml</p>
     * @param extras action event
     */
    @FXML
    public void changeSceneToExtraProducts(ActionEvent extras){
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(extras, "ExtrasMenu.fxml","Extra Products");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * This method obtains chosen coffees by the user
     * <p>
     *     It obtains all the coffees selected by
     *     the user and saves them in the list
     *     productsSelected in the class Order
     * </p>
     * @param event action event
     */
    @FXML void obtainChosenCoffees(ActionEvent event) {
        if (latte_drink.isSelected()){order.addProduct(new Product("Latte"));}
        if (espresso_drink.isSelected()){order.addProduct(new Product("Espresso"));}
        if (mocha_drink.isSelected()){order.addProduct(new Product("Mocha"));}
        if (irish_drink.isSelected()){order.addProduct(new Product("Irish"));}
        if (americano_drink.isSelected()){order.addProduct(new Product("Americano"));}
        if (frappe_drink.isSelected()){order.addProduct(new Product("Frappe"));}
        if (cocoa_drink.isSelected()){order.addProduct(new Product("Cocoa"));}
        changeSceneToExtraProducts(event);
    }




}



