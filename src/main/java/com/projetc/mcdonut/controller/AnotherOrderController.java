package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class is used to request another order.
 *
 * This class can have two purposes, if the customer wants to ask
 * for another order or if he wants to continue with the order.
 */
public class AnotherOrderController implements Initializable {

    @FXML
    Button newOrderMenu;

    @FXML
    Button skipUser;

    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * This is the method of changing the scene to WaitTime.
     *
     * <p>With this method we can change the scene with a try catch in the try
     * taking into account the necessary parameters to change the scene,
     * taking into account its event or action as a parameter of this specific scene "Wait Time!".</p>
     *
     * @param waitTime action event.
     */

    public void changeSceneContinue (ActionEvent waitTime) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(waitTime, "WaitTime.fxml","Wait Time!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is the method of changing the scene to Principal Menu.
     *
     * <p>With this method we can change the scene with a try catch in the try
     * taking into account the necessary parameters to change the scene,
     * taking into account its event or action as a parameter of this specific scene "Principal Menu!".</p>
     *
     * @param newOrder action event.
     */
    public void changeSceneNewOrder (ActionEvent newOrder) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(newOrder, "PrincipalMenu.fxml","Principal Menu");
            Order.setOrder(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
