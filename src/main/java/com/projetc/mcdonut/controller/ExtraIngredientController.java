package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
/***
 * This is the ExtraIngredientController class
 *
 * <p>
 *     This class is the controller of the fxml file
 *     ExtraIngredients.It obtains the extra ingredients
 *     chosen and adds them to the respective donut
 * </p>
 * @see Order
 * @author Elviscocho
 */
public class ExtraIngredientController implements Initializable {
    Order order;
    Product firstDonut;
    ArrayList<String> donutsWithRainbowSprinkles = new ArrayList<>();
    ArrayList<String> donutsWithChocolateSprinkles = new ArrayList<>();
    ArrayList<String> donutsWithOreo = new ArrayList<>();
    ArrayList<String> donutsWithGlazed = new ArrayList<>();
    ArrayList<String> donutsWithMAndMs = new ArrayList<>();
    @FXML
    private Label rainbow_sprinkles_label;
    @FXML private Label chocolate_sprinkles_label;
    @FXML private Label mAndMs_label;
    @FXML private Label glazed_label;
    @FXML private Label oreo_label;
    @FXML private ChoiceBox<String> rainbow_sprinkles_choiceBox = new ChoiceBox<>();
    @FXML private ChoiceBox<String> glazed_choiceBox = new ChoiceBox<>();
    @FXML private ChoiceBox<String> chocolate_sprinkles_choiceBox = new ChoiceBox<>();
    @FXML private ChoiceBox<String> oreo_choiceBox = new ChoiceBox<>();
    @FXML private ChoiceBox<String> mAndMs_choiceBox = new ChoiceBox<>();

    /**
     *This method sets text to the rainbow_sprinkle_label
     *
     * <p>
     *     It sets all the donuts that will have extra
     *     Rainbow sprinkle.
     * </p>
     * @param event action event
     */
    @FXML void setRainbowSprinklesLabel(ActionEvent event){
        String valueDonutChosen = rainbow_sprinkles_choiceBox.getValue();
        saveDonatWithRainbowSprinkles(valueDonutChosen);
        rainbow_sprinkles_label.setText(getItemsOnStringFromList(donutsWithRainbowSprinkles));
    }

    /**This method saves the given donut on list
     *
     * <p>It saves this methods on list  donutsWithRainbowSprinkles.
     * Then it changes the donuts price, according to the price of
     * the ingredient</p>
     *
     * @param valueDonut name of the donut
     */
    void saveDonatWithRainbowSprinkles(String valueDonut){
        donutsWithRainbowSprinkles.add(valueDonut);
        order.searchProductOnList(valueDonut).changePriceBySum(2);
    }
    /**
     *This method sets text to the glazed_label
     *
     * <p>
     *     It sets all the donuts that will have extra
     *     Glazed.
     * </p>
     * @param event action event
     */
    @FXML void setGlazedLabel(ActionEvent event){
        String valueDonutChosen = glazed_choiceBox.getValue();
        saveDonatWithGlazed(valueDonutChosen);
        glazed_label.setText(getItemsOnStringFromList(donutsWithGlazed));
    }
    /**This method saves the given donut on list
     *
     * <p>It saves this methods on list  donutsWithChocolateSprinkles.
     * Then it changes the donuts price, according to the price of
     * the ingredient</p>
     *
     * @param valueDonut name of the donut
     */
    void saveDonatWithGlazed(String valueDonut){
        donutsWithGlazed.add(valueDonut);
        order.searchProductOnList(valueDonut).changePriceBySum(3);
    }
    /**
     *This method sets text to the mAndMs_label
     *
     * <p>
     *     It sets all the donuts that will have extra
     *     M&M's.
     * </p>
     * @param event action event
     */
    @FXML void setMAndMsLabel(ActionEvent event){
        String valueDonutChosen = mAndMs_choiceBox.getValue();
        saveDonatWithMAndsMs(valueDonutChosen);
        mAndMs_label.setText(getItemsOnStringFromList(donutsWithMAndMs));
    }
    /**This method saves the given donut on list
     *
     * <p>It saves this methods on list  donutsWithRainbowSprinkles.
     * Then it changes the donuts price, according to the price of
     * the ingredient</p>
     *
     * @param valueDonut name of the donut
     */
    void saveDonatWithMAndsMs(String valueDonut){
        donutsWithMAndMs.add(valueDonut);
        order.searchProductOnList(valueDonut).changePriceBySum(2);
    }
    /**
     *This method sets text to the chocolate_sprinkles_label
     *
     * <p>
     *     It sets all the donuts that will have extra
     *     Chocolate glazed.
     * </p>
     * @param event action event
     */
    @FXML void setChocolateSprinklesLabel(ActionEvent event){
        String valueDonutChosen = chocolate_sprinkles_choiceBox.getValue();
        saveDonatWithChocolateSprinkles(valueDonutChosen);
        chocolate_sprinkles_label.setText(getItemsOnStringFromList(donutsWithChocolateSprinkles));
    }
    /**This method saves the given donut on list
     *
     * <p>It saves this methods on list  donutsWithRainbowSprinkles.
     * Then it changes the donuts price, according to the price of
     * the ingredient</p>
     *
     * @param valueDonut name of the donut
     */
    void saveDonatWithChocolateSprinkles(String valueDonut){
        donutsWithChocolateSprinkles.add(valueDonut);
        order.searchProductOnList(valueDonut).changePriceBySum(2);
    }
    /**
     *This method sets text to the oreo_label
     *
     * <p>
     *     It sets all the donuts that will have extra
     *     Oreo.
     * </p>
     * @param event action event
     */
    @FXML void setOreoLabel(ActionEvent event){
        String valueDonutChosen = oreo_choiceBox.getValue();
        saveDonatWithOreo(valueDonutChosen);
        oreo_label.setText(getItemsOnStringFromList(donutsWithOreo));
    }
    /**This method saves the given donut on list
     *
     * <p>It saves this methods on list  donutsWithRainbowSprinkles.
     * Then it changes the donuts price, according to the price of
     * the ingredient</p>
     *
     * @param valueDonut name of the donut
     */
    void saveDonatWithOreo(String valueDonut){
        donutsWithOreo.add(valueDonut);
        order.searchProductOnList(valueDonut).changePriceBySum(3);
    }

    /**
     * THis method returns a String o the Array items
     *
     * @param stringList the string to be used
     * @return a string wiht all the stringList items
     */
    public String getItemsOnStringFromList(ArrayList<String> stringList){
        String items = "";
        for (String word:
                stringList) {
            items += word +  "- ";
        }
        return items;
    }

    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * Here we initialize the order to the
     * clientsOrderList and all choiceboxes
     *
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects. This helps us when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = Order.getOrderClient();
        firstDonut = (Product) order.getProductsSelected().get(0);
        rainbow_sprinkles_choiceBox.getItems().addAll(order.getTitlesOfProductsSelected());
        glazed_choiceBox.getItems().addAll(order.getTitlesOfProductsSelected());
        chocolate_sprinkles_choiceBox.getItems().addAll(order.getTitlesOfProductsSelected());
        oreo_choiceBox.getItems().addAll(order.getTitlesOfProductsSelected());
        mAndMs_choiceBox.getItems().addAll(order.getTitlesOfProductsSelected());

        rainbow_sprinkles_choiceBox.setValue(firstDonut.getTitle());
        glazed_choiceBox.setValue(firstDonut.getTitle());
        chocolate_sprinkles_choiceBox.setValue(firstDonut.getTitle());
        oreo_choiceBox.setValue(firstDonut.getTitle());
        mAndMs_choiceBox.setValue(firstDonut.getTitle());
    }
    /***
     * This method changes a scene
     *
     * <p>This method changes specifically to the Coffee Menu
     * scene which is located in the file ExtraMenu.fxml</p>
     * @param coffee action event
     */
    @FXML
    void changSceneToCoffees(ActionEvent coffee) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(coffee, "CoffeeMenu.fxml","Coffee Menu");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
