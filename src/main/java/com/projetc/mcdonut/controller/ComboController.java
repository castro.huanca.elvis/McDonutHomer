package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the class of Combo Controller
 *
 *  It has its corresponding methods
 *  implementing 'Initializable'
 *  in order to have its initialization method
 */
public class ComboController implements Initializable {
    private Order order;

    @FXML
    private Button skip;
    @FXML
    private Label label;
    @FXML
    private CheckBox jeffson;
    @FXML
    private CheckBox valentine;
    @FXML
    private CheckBox homero;
    @FXML
    private CheckBox family;
    @FXML
    private CheckBox simpson;

    /**
     * It is a proper Java method that gives the order to initialize.
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = new Order();
    }

    /**
     * This is the method to change the scene to "CoffeeMenu".
     *
     * <p>This method helps us to change the scene, specifically to "CoffeeMenu",
     * we implement the 'Singleton' so we don't instantiate repeatedly,
     * to change the scene you need the action of the event, the name of the scene in fxml
     * and finally the title you want for the next scene, that parameter is from the changeScreenButtonPushed method.
     *
     * This method will only need the event action for changeScene.</p>
     *
     * @param coffeeMenu is the Action where the event will go.
     */

    public void changeSceneCoffeeMenu(ActionEvent coffeeMenu) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(coffeeMenu, "CoffeeMenu.fxml", "CoffeeMenu");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *This method creates a product when you select a combo.
     *
     * Depending on the combo chosen by the customer, a new product will be created.
     * When choosing the combo, a discount is given to the customer
     *
     * @param event This is the action performed when you click on Order in SceneBuilder.
     */
    @FXML
    void obtainChosenCombo (ActionEvent event) {
        Product product;

        if (jeffson.isSelected()) {
            order.addProduct(new Product("Jeffson Combo"));
            product = new Product("Jeffson Combo");
            product.setProductPrice((int) (product.getProductPrice()*0.05));
        }
        if (simpson.isSelected()) {
            order.addProduct(new Product("The simpsons Combo"));
            product = new Product("The simpsons Combo");
            product.setProductPrice((int) (product.getProductPrice()*0.05));
        }
        if (valentine.isSelected()) {
            order.addProduct(new Product("Valentine Combo"));
            product = new Product("Valentine Combo");
            product.setProductPrice((int) (product.getProductPrice()*0.1));
        }
        if (family.isSelected()) {
            order.addProduct(new Product("Family Combo"));
            product = new Product("Family Combo");
            product.setProductPrice((int) (product.getProductPrice()*0.1));
        }
        if (homero.isSelected()) {
            order.addProduct(new Product("homer Homerson Combo"));
        }
        changeSceneCoffeeMenu(event);

    }

    /**
     * This method provides information about Jeffson combo.
     *
     * The customer can visualize the details of Jeffson combo by clicking on the image of the combo.
     *
     * @param event This is the action is performed when you click on the combo image in SceneBuilder
     */
    @FXML
    void setJeffsonComboDetailsOnLabel(ActionEvent event) {
        label.setText(new Product("Jeffson Combo").getDetails());
    }

    /**
     * This method provides information about TheSimpsons combo.
     *
     * The customer can visualize the details of TheSimpsons combo by clicking on the image of the combo.
     *
     * @param event This is the action is performed when you click on the combo image in SceneBuilder
     */
    @FXML
    void setTheSimpsonsComboOnLabel(ActionEvent event) {
        label.setText(new Product("The simpsons Combo").getDetails());
    }

    /**
     * This method provides information about Valentine combo.
     *
     * The customer can visualize the details of Valentine combo by clicking on the image of the combo.
     *
     * @param event This is the action is performed when you click on the combo image in SceneBuilder
     */
    @FXML
    void setValentineComboOnLabel(ActionEvent event) {
        label.setText(new Product("Valentine Combo").getDetails());
    }

    /**
     * This method provides information about Family combo.
     *
     * The customer can visualize the details of Family combo by clicking on the image of the combo.
     *
     * @param event This is the action is performed when you click on the combo image in SceneBuilder
     */
    @FXML
    void setFamilyComboOnLabel(ActionEvent event) {
        label.setText(new Product("Family Combo").getDetails());
    }

    /**
     * This method provides information about homer-Homerson combo.
     *
     * The customer can visualize the details of homer-Homerson combo by clicking on the image of the combo.
     *
     * @param event This is the action is performed when you click on the combo image in SceneBuilder
     */
    @FXML
    void setHomeroOnLabel(ActionEvent event) {
        label.setText(new Product("homer Homerson Combo").getDetails());
    }
}