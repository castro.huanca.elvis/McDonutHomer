package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * This class is in charge of controlling
 * the scene of the modification of quantities.
 *
 * @see Order
 */
public class QuantityController implements Initializable {

    private Order order;

    @FXML
    private Button buttonModifyQuantity;

    @FXML
    private Button buttonShowInvoice;

    @FXML
    private Label labelProductSelected;

    @FXML
    private Label fullName;

    @FXML
    private Label nit;

    @FXML
    private Label totalPrice;

    @FXML
    private TableColumn<Product, String> columnDescription;

    @FXML
    private TableColumn<Product, Integer> columnPartialPrice;

    @FXML
    private TableColumn<Product, Integer> columnQuantity;

    @FXML
    private TableColumn<Product, Integer> columnUnitPrice;

    @FXML
    private TextField quantityProductText;

    @FXML
    private TableView<Object> tableViewDetailsOrder;

    private ObservableList <Object> productSelectedOrder;

    /**
     * This method loads data into the table.
     *
     * The data loaded in the table is obtained
     * from the arraylist created in the order
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        order = Order.getOrderClient();

        receiveOnlyNumbers();
        order.addProduct(new Product("Mocha"));
        order.addProduct(new Product("Latte"));
        productSelectedOrder = FXCollections.observableArrayList(order.getProductsSelected());

        this.columnDescription.setCellValueFactory(new PropertyValueFactory<Product, String>("title"));
        this.columnPartialPrice.setCellValueFactory(new PropertyValueFactory<Product, Integer>("partialPrice"));
        this.columnQuantity.setCellValueFactory(new PropertyValueFactory<Product, Integer>("quantity"));
        this.columnUnitPrice.setCellValueFactory(new PropertyValueFactory<Product, Integer>("productPrice"));

        tableViewDetailsOrder.setItems(productSelectedOrder);
    }

    /**
     * This method is responsible for
     * preventing the user from entering letters.
     *
     * When the user tries to enter letters,
     * they will not be entered in the text field.
     */
    private void receiveOnlyNumbers(){
        quantityProductText.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getText().matches("\\d+") || change.getText().equals("")) {
                return change;
            } else {
                change.setText("");
                change.setRange(
                        change.getRangeStart(),
                        change.getRangeStart()
                );
                return change;
            }
        }));
    }

    /**
     * This method modifies the quantity
     * of products in the order.
     *
     * Use the number entered in
     * the text field to modify the quantity
     *
     * @param event action event.
     */
    @FXML
    public void modifyQuantity(ActionEvent event){
        Product product = (Product) this.tableViewDetailsOrder.getSelectionModel().getSelectedItem();
        if(product == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("You must select a product");
            alert.showAndWait();
        }else{
            product.setQuantity(Integer.parseInt(quantityProductText.getText()));
            product.setPartialPrice(product.getQuantity() * product.getProductPrice());
        }

        tableViewDetailsOrder.refresh();

    }

    /**
     * This method fulfills the function of displaying the invoice.
     *
     * Get order and user information to show details, user, nit, price
     *
     * @param event an event.
     */
    @FXML
    public void showInvoice(ActionEvent event){
        quantityProductText.setVisible(false);
        buttonModifyQuantity.setVisible(false);
        buttonShowInvoice.setVisible(false);
        labelProductSelected.setVisible(false);

        fullName.setText("NAME: " + order.getCustomer().getFullName());
        nit.setText("NIT: " + order.getCustomer().getNit());
        totalPrice.setText("TOTAL PRICE" + sumPrices(order.getProductsSelected()));
    }

    /**
     * This method sums the prices of the products.
     *
     * Loop a list to sum the partial
     * prices (quantity * price) of all product.
     *
     * @param products for loop.
     * @return totalSum in format String.
     */
    private String sumPrices(ArrayList<Object> products){
        double totalSum = 0;
        for (Object objectIterator : products) {
            totalSum += ( (Product) objectIterator).getPartialPrice();
        }
        return String.valueOf(totalSum);
    }
}