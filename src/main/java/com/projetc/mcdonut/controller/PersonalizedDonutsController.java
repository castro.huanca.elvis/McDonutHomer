package com.projetc.mcdonut.controller;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * This is the Personalized Donuts Controller class.
 *
 * has an Initializable implementation
 * which has its initialize method.
 */
public class PersonalizedDonutsController implements Initializable {

    /**
     * It is a proper Java method that gives the order to initialize.
     *
     * @param url refers to an object.
     * @param resourceBundle when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
