package com.projetc.mcdonut.controller;

import com.projetc.mcdonut.model.Order;
import com.projetc.mcdonut.model.Product;
import com.projetc.mcdonut.util.ChangeScene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
/***
 * This is the ExtraProductController class
 *
 * <p>
 *     This class is the controller of the fxml file
 *     ExtrasMenu.It obtains the extra products chosen
 *     by the user and also shows its details
 * </p>
 * @see Order
 * @author Elviscocho
 */
public class ExtraProductController implements Initializable {
    private Order order;
    @FXML private CheckBox chocolate_flan_checkbox;
    @FXML private Label extrasDetailsLabel;
    @FXML private CheckBox green_jelly_checkbox;
    @FXML private CheckBox normal_flan_checkbox;
    @FXML private CheckBox red_jelly_checkbox;
    @FXML private CheckBox yellow_jelly_checkbox;
    /***
     * This method changes the extrasDetailsLabel
     *
     * <p>
     *     It sets the extrasDetailsLabel label
     *     to the Chocolate Flan specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setChocolate_flanDetailsOnLabel(ActionEvent event) {extrasDetailsLabel.setText(new Product("Chocolate Flan").getDetails());}

    /***
     * This method changes the extrasDetailsLabel
     *
     * <p>
     *     It sets the extrasDetailsLabel label
     *     to the Green Jelly specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setGreen_jellyDetailsOnLabel(ActionEvent event) {extrasDetailsLabel.setText(new Product("Green Jelly").getDetails());}

    /***
     * This method changes the extrasDetailsLabel
     *
     * <p>
     *     It sets the the extra products details label
     *     to the Vanilla Flan specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setNormal_flanDetailsOnLabel(ActionEvent event) {extrasDetailsLabel.setText(new Product("Vanilla Flan").getDetails());}

    /***
     * This method changes the extrasDetailsLabel
     *
     * <p>
     *     It sets the extrasDetailsLabel label
     *     to the Red Jelly specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setRed_jellyDetailsOnLabel(ActionEvent event) {extrasDetailsLabel.setText(new Product("Strawberry Jelly").getDetails());}

    /***
     * This method changes the extrasDetailsLabel
     *
     * <p>
     *     It sets the extrasDetailsLabel label
     *     to the Yellow Jelly specific details
     * </p>
     * @param event action event
     */
    @FXML
    void setYellow_jellyDetailsOnLabel(ActionEvent event) {extrasDetailsLabel.setText(new Product("Yellow Jelly").getDetails());}


    /***
     * This method changes a scene
     *
     * <p>This method changes specifically to the Quantity
     * scene which is located in the file Quantity.fxml</p>
     * @param quantity action event
     */
    @FXML
    void changSceneToQuantity(ActionEvent quantity) {
        try {
            ChangeScene.getChangeScene().changeScreenButtonPushed(quantity, "Quantity.fxml","Quantity");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /***
     * This method obtains chosen extra products by the user
     *
     * <p>
     *     It obtains all the extra products selected by
     *     the user and saves them in the list
     *     productsSelected in the class Order
     * </p>
     * @param event action event
     */
    @FXML
    void obtainChosenExtraProducts(ActionEvent event) {
        if (green_jelly_checkbox.isSelected()){order.addProduct(new Product("Green Jelly"));}
        if (normal_flan_checkbox.isSelected()){order.addProduct(new Product("Vanilla Flan"));}
        if (red_jelly_checkbox.isSelected()){order.addProduct(new Product("Strawberry Jelly"));}
        if (yellow_jelly_checkbox.isSelected()){order.addProduct(new Product("Yellow Jelly"));}
        if (chocolate_flan_checkbox.isSelected()){order.addProduct(new Product("Chocolate Flan"));}
        for (int i = 0; i < order.getproductsSelectedSize(); i++) {
            System.out.println(((Product) order.getProductsSelected().get(i)).getTitle());
        }
        changSceneToQuantity(event);
    }
    /**
     *It is a proper Java method that gives the order to initialize.
     *
     * Here we initialize the order to the clientsOrderList
     * @param url Class URL represents a Uniform Resource Locator,this is proper to java .
     * @param resourceBundle Resource packs contain locale-specific objects. This helps us when a necessary resource is needed.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        order = Order.getOrderClient();
    }
}
