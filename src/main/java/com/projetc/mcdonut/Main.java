package com.projetc.mcdonut;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;
/**
 * This is the main class.
 *
 * The Main class makes it compile the action
 * that was passed, extends from Application
 * which is proper to Java where it has its
 * start method.
 */

public class Main extends Application {

    /**
     * This method starts the scene.
     *
     * this method loads the windows so that the
     * the user can see a welcome coming from the
     * FXML, then the title of the window is seen
     * at the top, it is resized to the width and
     * length of the window and display.
     *
     * @param primaryStage is the main stage
     * @throw an exception if something goes wrong.
     */

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("Quantity.fxml")));
        primaryStage.setTitle("MC DONUTS");
        primaryStage.setScene(new Scene(root, 897, 634));
        primaryStage.show();
    }


    /**
     * This method run the program.
     *
     * @param args is an argument.
     */

    public static void main(String[] args) {
        launch(args);
    }
}